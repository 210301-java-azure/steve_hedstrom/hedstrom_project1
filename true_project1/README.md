# Computer Parts ECommerce
## Project Description
Computer Parts ECommerce is an application for tracking many things, including customer information, orders, shipments, and inventory.
## Technologies Used
- Java 1.8
- HTML/CSS
- JavaScript
- Hibernate
- Javalin
- Azure DB
## Features
- Track types of Central Processing Units.
- Login feature.
## Getting Started
https://gitlab.com/210301-java-azure/steve_hedstrom/hedstrom_project1.git
## Usage
Visit the website and sign-in. The initial window after signing in will show a list of the type of CPUs the company is tracking. At the top of the window, there are several CRUD buttons that gives you the ability to search for specific CPUs by ID, update CPUs, create new CPUs, and delete CPUs.
```python
API ENDPOINTS
GET http://52.175.205.207/cpus # returns all CPUs
GET http://52.175.205.207/cpus/:cpu_id # returns a specific CPU by its ID
POST http://52.175.205.207/cpus # creates a new CPU based on body parameters
PATCH http://52.175.205.207/cpus # updates a CPU based on body parameters
DELETE http://52.175.205.207/cpus/:cpu_id # removes a CPU based on its ID
```
## License
Computer Parts ECommerce provides an application for a company to sell their computer parts.
Copyright (C) <2021> Steven S. Hedstrom

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.