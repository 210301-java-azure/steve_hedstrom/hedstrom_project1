package steve.controllers;

import io.javalin.http.Context;
import io.javalin.http.UnauthorizedResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AuthenticationController {
    private final Logger logger = LoggerFactory.getLogger(AuthenticationController.class);

    public void authenticateLogin(Context ctx){
        String user = ctx.formParam("username");
        String pass = ctx.formParam("password");
        logger.info("{} attempted login", user);
        if(user!=null && user.equals("slhedstrom@gmail.com")){
            if(pass!=null && pass.equals("1!Dino5aur")){
                logger.info("successful login");
                ctx.header("Authorization", "admin-auth-token");
                ctx.status(200);
                return;
            }
            throw new UnauthorizedResponse("Password was incorrect");
        }
        throw new UnauthorizedResponse("Username was incorrect");
    }

    public void authorizeToken(Context ctx){
        logger.info("attempting to authorize token");

        if(ctx.method().equals("OPTIONS")){
            return;
        }

        String authHeader = ctx.header("Authorization");

        if(authHeader!=null && authHeader.equals("admin-auth-token")){
            logger.info("request is authorized, proceeding to handler method");
        } else {
            logger.warn("improper authorization");
            throw new UnauthorizedResponse();
        }
    }

}
