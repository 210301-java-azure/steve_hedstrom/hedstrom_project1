package steve.controllers;

import io.javalin.http.BadRequestResponse;
import io.javalin.http.Context;
import io.javalin.http.NotFoundResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import steve.models.CPU;
import steve.services.CpuService;
import java.util.List;

public class CpuController {
    private final Logger logger= LoggerFactory.getLogger(CpuController.class);
    private CpuService service=new CpuService();

    public void handleGetAllCpus(Context ctx){
        logger.info("requesting all CPUs");
        ctx.json(service.getAllCpus());
    }

    public void handleGetCpuById(Context ctx){
        String idString=ctx.pathParam("cpuid");
        if(idString.matches("^\\d+$")){
            int idInput=Integer.parseInt(idString);
            CPU cpu=service.getCpuById(idInput);
            if(cpu==null){
                logger.warn("no cpu present with id: {}", idInput);
                throw new NotFoundResponse("No item found with provided ID: "+idInput);
            }else{
                logger.info("getting item with id: {}", idInput);
                ctx.json(cpu);
            }
        }else{
            throw new BadRequestResponse("input \""+idString+"\" cannot be parsed to an int");

        }
    }

    public void handlePostNewCpu(Context ctx){
        CPU cpu=ctx.bodyAsClass(CPU.class);
        logger.info("adding new CPUs: {}", cpu);
        service.addNewCpu(cpu);
        ctx.status(201);
    }

    public void handleDeleteCpuById(Context ctx){
        String idString=ctx.pathParam("cpuid");
        if(idString.matches("^\\d+$")){
            int idInput=Integer.parseInt(idString);
            logger.warn("deleting CPU with id: {}", idInput);
            service.deleteCpu(idInput);
        }else{
            throw new BadRequestResponse("input \""+idString+"\"cannot be parsed to an int");
        }
    }

    public void handleUpdateCpuById(Context ctx){
        CPU cpu=ctx.bodyAsClass(CPU.class);
        logger.warn("updating CPU with id: {}", cpu);
        service.updateCpu(cpu);
    }

    public void handleGetCpuInPriceRange(Context ctx){
        String max=ctx.formParam("min");
        String min=ctx.formParam("max");
        if(max!=null||min!=null){
            logger.info("getting CPUs within price range");
            ctx.json(service.getCpuInPriceRange(min, max));
        }
    }

    public void handleGetCpuByCores(Context ctx){
        String coreString=ctx.pathParam("cores");
        if(coreString.matches("^\\d+$")){
            int coreInput=Integer.parseInt(coreString);
            List<CPU> cpu=service.getCpuByCores(coreInput);
            if(cpu==null){
                logger.warn("no cpu present with {} cores", coreInput);
                throw new NotFoundResponse("No item found with provided ID: "+coreInput);
            }else{
                logger.info("getting CPUs with {} cores", coreInput);
                ctx.json(cpu);
            }
        }else{
            throw new BadRequestResponse("input \""+coreString+"\" cannot be parsed to an int");

        }
    }
}