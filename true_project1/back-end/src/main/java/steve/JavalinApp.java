package steve;

import io.javalin.Javalin;
import io.javalin.core.JavalinConfig;
import steve.controllers.AuthenticationController;
import steve.controllers.CpuController;
import steve.controllers.CustomerController;

import static io.javalin.apibuilder.ApiBuilder.*;

public class JavalinApp {

    AuthenticationController authenticationController = new AuthenticationController();
    CpuController cpuController=new CpuController();
    CustomerController customerController=new CustomerController();

    /*JWTProvider provider= Provider.createHMAC256();
    Handler decodeHandler = JavalinJWT.createHeaderDecodeHandler(provider);

    static Map<String, Role> rolesMapping = new HashMap<>();
    static{
       rolesMapping.put("user", Roles.USER);
       rolesMapping.put("admin", Roles.ADMIN);
    }*/

        Javalin app = Javalin.create(JavalinConfig::enableCorsForAllOrigins).routes(()->{
            //Full range CRUD on CPUs.
            path("cpus", ()->{
                before("", authenticationController::authorizeToken);
                get(cpuController::handleGetAllCpus);
                post(cpuController::handlePostNewCpu);
                patch(cpuController::handleUpdateCpuById);
                path(":cpuid", ()->{
                    before("", authenticationController::authorizeToken);
                    get(cpuController::handleGetCpuById);
                    delete(cpuController::handleDeleteCpuById);
                });
            });
            //Get CPUs by cores count.
            path("cpu-cores/:cores", ()->{
                before("", authenticationController::authorizeToken);
                get(cpuController::handleGetCpuByCores);
            });
            //In-development. Get CPUs by price range.
            path("cpuprice", ()->{
                before("", authenticationController::authorizeToken);
                get(cpuController::handleGetCpuInPriceRange);
            });
            //Full range CRUD on Customers.
            path("customers", ()->{
                before("", authenticationController::authorizeToken);
                get(customerController::handleGetAllCustomers);
                post(customerController::handlePostNewCustomer);
                patch(customerController::handleUpdateCustomerUsername);
                path(":customerid", ()->{
                    before("", authenticationController::authorizeToken);
                    get(customerController::handleGetCustomerById);
                    delete(customerController::handleDeleteCustomerById);
                });
                path(":username", ()->{
                    before("", authenticationController::authorizeToken);
                    get(customerController::handleGetCustomerByUsername);
                });
            });
            //Login
            path("login", () -> post(authenticationController::authenticateLogin));
        });

        public void start(int port){
            this.app.start(port);
        }

        public void stop(){
            this.app.stop();
        }
}