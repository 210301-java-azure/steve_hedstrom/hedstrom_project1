package steve.data;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import steve.models.CPU;
import steve.util.HibernateUtil;

import java.util.List;

public class CpuDaoHibImplementation implements CpuDao {
    private final Logger logger = LoggerFactory.getLogger(CpuDaoHibImplementation.class);

    @Override
    public List<CPU> getAllCpus() {
        try(Session s=HibernateUtil.getSession()) {
            List<CPU> cpus=s.createQuery("from CPU", CPU.class).list();
            return cpus;
        }
    }

    @Override
    public List<CPU> getCpuInPriceRange(double min, double max) {
        /*try(Session s=HibernateUtil.getSession()){
            CPU cpu=s.createQuery("from CPU where cpuId=?", CPU.class);
        }*/
        return null;
    }

    @Override
    public CPU getCpuById(int cpuId) {
        try(Session s=HibernateUtil.getSession()){
            CPU cpu=s.get(CPU.class, cpuId);
            logger.info("getting cpu: {}", cpu);
            return cpu;
        }
    }

    @Override
    public List<CPU> getCpuByCores(int cores) {
        return null;
    }

    @Override
    public void addNewCpu(CPU cpu) {
        try(Session s=HibernateUtil.getSession()){
            Transaction tx=s.beginTransaction();
            int cpuId=(int)s.save(cpu);
            cpu.setCpuId(cpuId);
            logger.info("added new CPU with id: {}", cpu);
            tx.commit();
        }
    }

    @Override
    public void deleteCpu(int cpuId) {
        try(Session s=HibernateUtil.getSession()){
            Transaction tx=s.beginTransaction();
            s.delete(new CPU (cpuId));
            logger.info("deleted CPU with id: {}", cpuId);
            tx.commit();
        }
    }

    @Override
    public void updateCpu(CPU cpu) {
        try(Session s=HibernateUtil.getSession()){
            Transaction tx=s.beginTransaction();
            s.saveOrUpdate(cpu);
            logger.info("updated CPU with id: {}", cpu.getCpuId());
            tx.commit();
        }
    }
}
