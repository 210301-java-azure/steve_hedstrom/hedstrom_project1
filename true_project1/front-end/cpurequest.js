const xhr=new XMLHttpRequest();
xhr.open("GET", "http://52.175.205.207/cpus");
xhr.setRequestHeader("Authorization", "admin-auth-token");
xhr.onreadystatechange=function(){
    if(xhr.readyState==4){
        if(xhr.status==200){
        const cpus=JSON.parse(xhr.responseText);
        renderCpusInTable(cpus);
        }else{
            console.log("Something went wrong with the request. Status= "+xhr.status+" "+xhr.statusText);
        }
    }
}
xhr.send();

function renderCpusInTable(cpuList){
    const tableBody=document.getElementById("cpu-table");
    tableBody.hidden=false;
    for(let cpu of cpuList){
        let newRow=document.createElement("tr");
        newRow.innerHTML=`<td>${cpu.cpuId}</td><td>${cpu.cpuName}</td><td>${cpu.cores}</td><td>${cpu.ghz}</td><td>${cpu.price}</td>`;
        tableBody.appendChild(newRow);
    }
    console.log(cpuList);
}