function createCpu(){
    const jname=document.getElementById("name-input").value;
    const jcores=document.getElementById("cores-input").value;
    const jghz=document.getElementById("ghz-input").value;
    const jprice=document.getElementById("price-input").value;
    console.log(jname+" "+jcores+" "+jghz+" "+jprice);
    var xmlhttp=new XMLHttpRequest();
    xmlhttp.open("POST", "http://52.175.205.207/cpus");
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.setRequestHeader("Authorization", "admin-auth-token");
    xmlhttp.send(JSON.stringify({cpuName:jname, cores:jcores, ghz:jghz, price:jprice}));
}