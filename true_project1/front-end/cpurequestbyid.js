function renderCpuById(){
	var part=document.getElementById("enter-cpu-by-id").value;
    var xmlhttp=new XMLHttpRequest();
	xmlhttp.open("GET", "http://52.175.205.207/cpus/"+part);
	xmlhttp.setRequestHeader("Authorization", "admin-auth-token");
    xmlhttp.onreadystatechange=function(){
		console.log("Ready state= "+xmlhttp.readyState+"status= "+xmlhttp.status+" "+xmlhttp.statusText);
        if(xmlhttp.readyState==4&&xmlhttp.status==200){
			var cpu=JSON.parse(xmlhttp.responseText);
			console.log(cpu)
            getCpuById(cpu);
        }else{
			console.log("something went wrong with the request. status= "+xmlhttp.status+" "+xmlhttp.statusText);
		}	
    }
	xmlhttp.send();
}

function getCpuById(cpu){
	var table=document.getElementById("cpu-by-id");
	table.hidden=false;
	var newRow=document.createElement("tr");
	newRow.innerHTML=`<td>${cpu.cpuId}</td><td>${cpu.cpuName}</td><td>${cpu.cores}</td><td>${cpu.ghz}</td><td>${cpu.price}</td>`;
	table.appendChild(newRow);
	console.log(newRow);
}